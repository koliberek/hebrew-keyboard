﻿; ========== Nie ruszać  ====================
;
#SingleInstance Force
#InstallKeybdHook

if ( ! A_IsCompiled)
{
	Menu, Tray, Icon,,,1
	Menu, Tray, Icon, %A_ScriptDir%\ico\alef.ico,1,1
}
;; ============================================

menu, tray, NoStandard
; Menu, Tray, Add  ; Creates a separator line.
Menu, Tray, Add, Pomoc, tuHelp ; Creates a new menu item.
Menu, Tray, Add, Wyjście, closeapp ; Creates a new menu item.
Menu, Tray, Default , Pomoc
Menu, Tray, Click, 1

;; ===== help  ==============

Gui, next:new
Gui, next:Font, s11 bold, Tahoma
Gui, next:Add, Text,x10 y0 w140 h20, litera hebrajska
Gui, next:Add, Text,x170 y0 w150 h20, klawisz


Gui, next:Font, s18 normal, Times New Roman
Gui, next:Add, Text,x10 y20 w20 h20, א
Gui, next:Font, s11, Tahoma
Gui, next:Add, Text,x40 y22 w140 h20, alef
Gui, next:Add, Text,x170 y22 w150 h20, a
Gui, next:Font, s18, Times New Roman

Gui, next:Font, s18, Times New Roman
Gui, next:Add, Text,x10 y40, ב
Gui, next:Font, s11, Tahoma
Gui, next:Add, Text,x40 y42 w140 h20, bet
Gui, next:Add, Text,x170 y42 w150 h20, b

Gui, next:Font, s18, Times New Roman
Gui, next:Add, Text,x10 y60, ג
Gui, next:Font, s11, Tahoma
Gui, next:Add, Text,x40 y62 w140 h20, gimel
Gui, next:Add, Text,x170 y62 w150 h20, g

Gui, next:Font, s18, Times New Roman
Gui, next:Add, Text,x10 y80, ד
Gui, next:Font, s11, Tahoma
Gui, next:Add, Text,x40 y82 w140 h20, dalet
Gui, next:Add, Text,x170 y82 w150 h20, d

Gui, next:Font, s18, Times New Roman
Gui, next:Add, Text,x10 y100, ה
Gui, next:Font, s11, Tahoma
Gui, next:Add, Text,x40 y102 w140 h20, he
Gui, next:Add, Text,x170 y102 w150 h20, h

Gui, next:Font, s18, Times New Roman
Gui, next:Add, Text,x10 y120, ו
Gui, next:Font, s11, Tahoma
Gui, next:Add, Text,x40 y122 w140 h20, waw
Gui, next:Add, Text,x170 y122 w150 h20, w

Gui, next:Font, s18, Times New Roman
Gui, next:Add, Text,x10 y140, ז
Gui, next:Font, s11, Tahoma
Gui, next:Add, Text,x40 y142 w140 h20, zain
Gui, next:Add, Text,x170 y142 w150 h20, z

Gui, next:Font, s18, Times New Roman
Gui, next:Add, Text,x10 y160, ח
Gui, next:Font, s11, Tahoma
Gui, next:Add, Text,x40 y162 w140 h20, het
Gui, next:Add, Text,x170 y162 w150 h20, Shift + h

Gui, next:Font, s18, Times New Roman
Gui, next:Add, Text,x10 y180, י
Gui, next:Font, s11, Tahoma
Gui, next:Add, Text,x40 y182 w140 h20, jod
Gui, next:Add, Text,x170 y182 w150 h20, j

Gui, next:Font, s18, Times New Roman
Gui, next:Add, Text,x10 y200, כ
Gui, next:Font, s11, Tahoma
Gui, next:Add, Text,x40 y202 w140 h20, kaf
Gui, next:Add, Text,x170 y202 w150 h20, k

Gui, next:Font, s18, Times New Roman
Gui, next:Add, Text,x10 y220, ך
Gui, next:Font, s11, Tahoma
Gui, next:Add, Text,x40 y222 w140 h20, kaf sofit
Gui, next:Add, Text,x170 y222 w150 h20, Ctrl + k

Gui, next:Font, s18, Times New Roman
Gui, next:Add, Text,x10 y240, ל
Gui, next:Font, s11, Tahoma
Gui, next:Add, Text,x40 y242 w140 h20, lamed
Gui, next:Add, Text,x170 y242 w150 h20, l

Gui, next:Font, s18, Times New Roman
Gui, next:Add, Text,x10 y260, מ
Gui, next:Font, s11, Tahoma
Gui, next:Add, Text,x40 y262 w140 h20, mem
Gui, next:Add, Text,x170 y262 w150 h20, m

Gui, next:Font, s18, Times New Roman
Gui, next:Add, Text,x10 y280, ם
Gui, next:Font, s11, Tahoma
Gui, next:Add, Text,x40 y282 w140 h20, mem sofit
Gui, next:Add, Text,x170 y282 w150 h20, Ctrl + m

Gui, next:Font, s18, Times New Roman
Gui, next:Add, Text,x10 y300, נ
Gui, next:Font, s11, Tahoma
Gui, next:Add, Text,x40 y302 w140 h20, nun
Gui, next:Add, Text,x170 y302 w150 h20, n

Gui, next:Font, s18, Times New Roman
Gui, next:Add, Text,x10 y320, ן
Gui, next:Font, s11, Tahoma
Gui, next:Add, Text,x40 y322 w140 h20, nun sofit
Gui, next:Add, Text,x170 y322 w150 h20, Ctrl + n

Gui, next:Font, s18, Times New Roman
Gui, next:Add, Text,x10 y340, ס
Gui, next:Font, s11, Tahoma
Gui, next:Add, Text,x40 y342 w140 h20, samek
Gui, next:Add, Text,x170 y342 w150 h20, s

Gui, next:Font, s18, Times New Roman
Gui, next:Add, Text,x10 y360, ע
Gui, next:Font, s11, Tahoma
Gui, next:Add, Text,x40 y362 w140 h20, ajin
Gui, next:Add, Text,x170 y362 w150 h20, y

Gui, next:Font, s18, Times New Roman
Gui, next:Add, Text,x10 y380, פ
Gui, next:Font, s11, Tahoma
Gui, next:Add, Text,x40 y382 w140 h20, pe
Gui, next:Add, Text,x170 y382 w150 h20, p

Gui, next:Font, s18, Times New Roman
Gui, next:Add, Text,x10 y400, צ
Gui, next:Font, s11, Tahoma
Gui, next:Add, Text,x40 y402 w140 h20, cade
Gui, next:Add, Text,x170 y402 w150 h20, c

Gui, next:Font, s18, Times New Roman
Gui, next:Add, Text,x10 y420, ץ
Gui, next:Font, s11, Tahoma
Gui, next:Add, Text,x40 y422 w140 h20, cade sofit
Gui, next:Add, Text,x170 y422 w150 h20, Ctrl + c

Gui, next:Font, s18, Times New Roman
Gui, next:Add, Text,x10 y440, ק
Gui, next:Font, s11, Tahoma
Gui, next:Add, Text,x40 y442 w140 h20, kof
Gui, next:Add, Text,x170 y442 w150 h20, q

Gui, next:Font, s18, Times New Roman
Gui, next:Add, Text,x10 y460, ר
Gui, next:Font, s11, Tahoma
Gui, next:Add, Text,x40 y462 w140 h20, resz
Gui, next:Add, Text,x170 y462 w150 h20, r

Gui, next:Font, s18, Times New Roman
Gui, next:Add, Text,x10 y480, ש
Gui, next:Font, s11, Tahoma
Gui, next:Add, Text,x40 y482 w140 h20, szin
Gui, next:Add, Text,x170 y482 w150 h20, Shift + s

Gui, next:Font, s18, Times New Roman
Gui, next:Add, Text,x10 y500, ת
Gui, next:Font, s11, Tahoma
Gui, next:Add, Text,x40 y502 w140 h20, taw
Gui, next:Add, Text,x170 y502 w150 h20, Shift + t

; ==== samogłoski ====

Gui, next:Font, s11 bold, Tahoma
Gui, next:Add, Text,x320 y0 w140 h20, samogłoska
Gui, next:Add, Text,x420 y0 w150 h20, klawisz

Gui, next:Font, s18 normal, Times New Roman
Gui, next:Add, Text,x320 y20 w20 h30, אָ
Gui, next:Font, s11, Tahoma
Gui, next:Add, Text,x345 y25 w140 h20, qamec
Gui, next:Add, Text,x425 y25 w150 h20, Alt + a

Gui, next:Font, s18 normal, Times New Roman
Gui, next:Add, Text,x320 y50 w20 h30, אַ
Gui, next:Font, s11, Tahoma
Gui, next:Add, Text,x345 y55 w140 h20, patach
Gui, next:Add, Text,x425 y55 w150 h20, Alt + Shift + a

Gui, next:Font, s18 normal, Times New Roman
Gui, next:Add, Text,x320 y80 w20 h30, אֵ
Gui, next:Font, s11, Tahoma
Gui, next:Add, Text,x345 y85 w140 h20, cere
Gui, next:Add, Text,x425 y85 w150 h20, e

Gui, next:Font, s18 normal, Times New Roman
Gui, next:Add, Text,x320 y110 w20 h30, אֶ
Gui, next:Font, s11, Tahoma
Gui, next:Add, Text,x345 y115 w140 h20, segol
Gui, next:Add, Text,x425 y115 w150 h20, Shift + e

Gui, next:Font, s18 normal, Times New Roman
Gui, next:Add, Text,x320 y140 w20 h30, אִי
Gui, next:Font, s11, Tahoma
Gui, next:Add, Text,x345 y145 w140 h20, hirek jod
Gui, next:Add, Text,x425 y145 w150 h20, i

Gui, next:Font, s18 normal, Times New Roman
Gui, next:Add, Text,x320 y170 w20 h30, אִ
Gui, next:Font, s11, Tahoma
Gui, next:Add, Text,x345 y175 w140 h20, hirek
Gui, next:Add, Text,x425 y175 w150 h20, Shift + i

Gui, next:Font, s18 normal, Times New Roman
Gui, next:Add, Text,x320 y200 w20 h30, אֹ
Gui, next:Font, s11, Tahoma
Gui, next:Add, Text,x345 y205 w140 h20, holem
Gui, next:Add, Text,x425 y205 w150 h20, o

Gui, next:Font, s18 normal, Times New Roman
Gui, next:Add, Text,x320 y230 w20 h30, אוֹ
Gui, next:Font, s11, Tahoma
Gui, next:Add, Text,x345 y235 w140 h20, holem vav
Gui, next:Add, Text,x425 y235 w150 h20, Shift + o

Gui, next:Font, s18 normal, Times New Roman
Gui, next:Add, Text,x320 y260 w20 h30, אוּ
Gui, next:Font, s11, Tahoma
Gui, next:Add, Text,x345 y265 w140 h20, szurek
Gui, next:Add, Text,x425 y265 w150 h20, u

Gui, next:Font, s18 normal, Times New Roman
Gui, next:Add, Text,x320 y290 w20 h30, אֻ
Gui, next:Font, s11, Tahoma
Gui, next:Add, Text,x345 y295 w140 h20, kibuc
Gui, next:Add, Text,x425 y295 w150 h20, Shift + u

Gui, next:Font, s18 normal, Times New Roman
Gui, next:Add, Text,x320 y320 w20 h30, ּ
Gui, next:Font, s11, Tahoma
Gui, next:Add, Text,x345 y325 w140 h20, dagesh
Gui, next:Add, Text,x425 y325 w150 h20, . (kropka)

Gui, next:Font, s18 normal, Times New Roman
Gui, next:Add, Text,x320 y350 w20 h30, ְ
Gui, next:Font, s11, Tahoma
Gui, next:Add, Text,x345 y355 w140 h20, szewa
Gui, next:Add, Text,x425 y355 w150 h20, : (dwukropek)

;; === koniec helpa ===

$a:: ; alef
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send א
	}
	if state = 0
	{
		Send, a
	}
return

$b:: ; bet
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send ב
	}
	if state = 0
	{
		Send, b
	}
return

$g:: ; gimel
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send ג
	}
	if state = 0
	{
		Send, g
	}
return

$d:: ; dalet
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send ד
	}
	if state = 0
	{
		Send, d
	}
return


$h:: ; he
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send ה
	}
	if state = 0
	{
		Send, h
	}
return

$w:: ; waw
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send ו
	}
	if state = 0
	{
		Send, w
	}
return

$z:: ; zajin
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send ז
	}
	if state = 0
	{
		Send, z
	}
return

$+h:: ; het
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send ח
	}
	if state = 0
	{
		Send, +h
	}
return

$t:: ; tet
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send ט
	}
	if state = 0
	{
		Send, t
	}
return

$j:: ; jod
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send י
	}
	if state = 0
	{
		Send, j
	}
return

$k:: ; kaf
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send כ
	}
	if state = 0
	{
		Send, k
	}
return


$^k:: ; kaf sofit
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send ך
	}
	if state = 0
	{
		Send, ^k
	}
return

$l:: ; lamed
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send ל
	}
	if state = 0
	{
		Send, l
	}
return

$^m:: ; mem sofit
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send ם
	}
	if state = 0
	{
		Send, ^m
	}
return

$m:: ; mem
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send מ
	}
	if state = 0
	{
		Send, m
	}
return

$n:: ; nun
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send נ
	}
	if state = 0
	{
		Send, n
	}
return

$^n:: ; nun sofit
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send ן
	}
	if state = 0
	{
		Send, ^n
	}
return

$s:: ; samech
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send ס
	}
	if state = 0
	{
		Send, s
	}
return

$y:: ; ayin
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send ע
	}
	if state = 0
	{
		Send, y
	}
return

$^p:: ; pe sofit
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send ף
	}
	if state = 0
	{
		Send, ^p
	}
return

$p:: ; pe
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send פ
	}
	if state = 0
	{
		Send, p
	}
return

$c:: ; cade
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send צ
	}
	if state = 0
	{
		Send, c
	}
return

$^c:: ; cade sofit
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send ץ
	}
	if state = 0
	{
		Send, ^c
	}
return

$q:: ; kof
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send ק
	}
	if state = 0
	{
		Send, q
	}
return

$r:: ; resz
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send ר
	}
	if state = 0
	{
		Send, r
	}
return

$+s:: ; sin
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send ש
	}
	if state = 0
	{
		Send, +s
	}
return

$+t:: ; taw
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send ת
	}
	if state = 0
	{
		Send, T
	}
return

$.:: ; dagesz
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send ּ
	}
	if state = 0
	{
		Send, .
	}
return

$+SC27:: ; szewa
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send ְ
	}
	if state = 0
	{
		Send, :
	}
return

F1::
state := GetKeyState("ScrollLock", "T")
if state = 1
{
	tuHelp:
	Gui, next:Show,Autosize, Litery alfabetu hebrajskiego
	return
}
if state = 0
{
	Send, {F1}
}

return

; klawisze z samogłoskami

$!a:: ; qamec
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send ָ
	}
	if state = 0
	{
		Send, a
	}
return

$!+a:: ; patah
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send ַ
	}
	if state = 0
	{
		Send, !+a
	}
return

$e:: ; tsere
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send ֵ
	}
	if state = 0
	{
		Send, e
	}
return

$+e:: ; segol
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send ֶ
	}
	if state = 0
	{
		Send, +e
	}
return

$i:: ; hirek jod
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send ִי	
	}
	if state = 0
	{
		Send, i
	}
return

$+i:: ; hirek
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send ִ	
	}
	if state = 0
	{
		Send, +i
	}
return

$o:: ; holem
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send ֹ
	}
	if state = 0
	{
		Send, o
	}
return

$+o:: ; holem waw
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send וֹ
	}
	if state = 0
	{
		Send, +o
	}
return

$u:: ; szurek
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send וּ
	}
	if state = 0
	{
		Send, u
	}
return

$+u:: ; kibuc
	state := GetKeyState("ScrollLock", "T")
	if state = 1
	{
		Send ֻ
	}
	if state = 0
	{
		Send, +u
	}
return

#IfWinActive, Litery alfabetu hebrajskiego
	Esc::
		Gui, next:Hide
	return


	closeapp:
	ExitApp
	return