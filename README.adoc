# mikledet (hebr. klawiatura)

Program służy do wprowadzania hebrajskich liter za pomocą klawiatury w układzie QWERTY.
Po włączeniu ScrollLocka niektóre klawisze lub kombinacje klawiszy powodują wpisanie hebrajskiego znaku. Znaki są wpisywane od prawej do lewej strony.

Uruchomiony program umieszcza ikonę w tzw. "szufladzie systemowej" (obok zegara). Kliknięcie tej ikony lewym klawiszem myszy spowoduje pokazanie ściągawki znaków i klawiszy (takiej jak poniżej).  Kliknięcie ikony prawym klawiszem myszy otworzy menu kontekstowe, w którym znajduje się pomoc (ściągawka) oraz wyjście - wyłączenie programu.

*Program do pobrania:* https://gitlab.com/koliberek/hebrew-keyboard/-/blob/main/build/mikledet_1.0.exe[mikledet_1.0.exe]

Oto klawisze odpowiadające za znaki hebrajskie:

Ogólna zasada: +
litera = znak bez dodatków +
litera + Alt = znak z dageszem +
litera + Ctrl = sofit +
Wyjątek stanowią szin i taw, które są wpisywane z shiftem ponieważ odpowiadające im litery na klawiaturze są już wykorzystane do liter samek i tet.

[%header,cols="^1,4,4"]
|===
3+^|Spółgłoski
2+| Litera hebrajska | Skrót klawiszowy
|[hz]#א# |alef			| a
|[hz]#ב# |bet			| b
|[hz]#ג# |gimel			| g
|[hz]#ד# |dalet			| d
|[hz]#ה# |he			| h
|[hz]#ו# |waw			| w
|[hz]#ז# |zain			| z
|[hz]#ח# |het			| Shift + h
|[hz]#ט# |tet			| t
|[hz]#י# |jod			| j
|[hz]#כ# |kaf			| k
|[hz]#ך# |kaf sofit		| Ctrl+ k
|[hz]#ל# |lamed			| l
|[hz]#ם# |mem sofit		| Ctrl + m
|[hz]#מ# |mem			| m
|[hz]#נ# |nun			| n
|[hz]#ן# |nun sofit		| n
|[hz]#ס# |samek			| s
|[hz]#ע# |ajin			| y
|[hz]#ף# |pe sofit		| Ctrl + p
|[hz]#פ# |pe			| p
|[hz]#צ# |cade			| c
|[hz]#ץ# |cade sofit		| Ctrl + c
|[hz]#ק# |kof			| q
|[hz]#ר# |resz			| r
|[hz]#ש# |szin			| Shift + s
|[hz]#ת# |taw			| Shift + t
|===

[%header,cols="^1,4,4"]
|===
3+^|Samogłoski
2+|Samogłoska|Skrót klawiszowy
|אָ|qamec|Alt+a
|אַ|patach|Alt + Shit + a
|אֵ|cere|e
|אֶ|segol|Shift + e
|אִי|hirek jod|i
|אִ|hirek|Shift + i
|אֹ|holem|o
|אוֹ|holem vav|Shift + o
|אוּ|szurek|u
|אֻ|kibuc|Shift + u
|===
